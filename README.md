# Home of the free study activity: C++11/14 introduction. #

On this repository you will find the wiki containing information about the
course and in addition we will make available slides and other material
relevant.

* Go to the
  [wiki](https://bitbucket.org/mobiledevaau/cpp-intro-freestudy/wiki) for
  an overview.