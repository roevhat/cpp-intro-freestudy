/**
 * ----------------------------------------------------------------------------
 * EXAM HAND-IN
 * ----------------------------------------------------------------------------
 * Name: Joakim Børlum Petersen
 * Mail: jopete12@student.aau.dk
 * ----------------------------------------------------------------------------
 * LIST OF FEATURES
 * ----------------------------------------------------------------------------
 * - Direct initialization
 * - Direct map initialization
 * - Ranged loop m. auto/object ref.
 * - Smart pointer / shared pointer resource management
 * - Constexpr for compile time evaluation
 * - Static assertion for compile time assertion
 * - Member initialization + default parameter
 * - The std::function for storing callable objects
 */

#include <iostream>
#include <vector>
#include <map>
#include <string>
#include <cassert>
#include <memory>
#include <functional>

/*Return constant expression for calculation at compile time*/
constexpr int sum(int a, int b)
{
    return a + b;
}

struct shape
{
    virtual double area() = 0;
};

struct square : public shape
{
    square(int side_length) : m_side_length(side_length)
    {
    	area(); /*Make sure we call area!*/
    }

    /*If constructed without arguments, call square with 0!*/
    square() : square(0)
    { }

    double area()
    {
        return m_side_length * m_side_length;
    }

    int m_side_length;
};

int main()
{
    // Use at least 5 different modern C++11/14 features to update the following code

    // Question: fill the vector with values 1-10
    /*We now use direct list initialization*/
    std::vector<int> v = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

    // Question: Initialize mymap
    /*We now use direct map initialization*/
    std::map<char,int> mymap = {{'a', 200}, {'b', 100}, {'c', 300}};

    // Question: show content of mymap:
    /*We use the awesome 'auto' of c++11 :-)*/
    for (auto& i : mymap) 
    	std::cout << i.first << " => " << i.second << std::endl;

    // Question: Use smart_pointers to handle resource allocation
    /*We use a shared pointer instead...*/
    std::shared_ptr<std::string> course(new std::string("C++11/14 free study activity"));
 
    if (course)
    {
        course = nullptr;
    }

    // Question: Calculate value at compile time
    constexpr int number = sum(2,3);

    // Question: Make sure a short is 2 bytes at compile time
    /*We use static_assert for compile time assertion!*/
    static_assert(sizeof(short) == 2, "type [short] != 2 bytes!");

    // Question: Make sure we override area and initialize m_side_length
    /* => See line [21;28]*/
    square s(3);

    // Question: Call sum using a std::function
    /*We make ourselves a cool_function (std::function) to call sum!*/
    std::function<int(int,int)> cool_function = sum;
    int mysum = cool_function(3,4);

    return 0;
}
