#include <iostream>

namespace aau
{
    void print()
    {
        std::cout << "hello c++" << std::endl;
    }
}

int main()
{
    int loop = 0;

    // I could also implement this using a for loop
    while(loop < 10)
    {
        aau::print();
    }

    return 0;
}
