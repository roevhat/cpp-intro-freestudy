# Overview

In these files we take a basic tour of C++, the examples will mostly cover
functionality avilable in C++03.

* main.cpp
    * Minmal program, close to C
    * Include C++ header (no extension)

* helloworld.cpp
    * Using the C++ iostream library to print to standard out
    * `cout` and `endl` are two nested types inside `std`
    * In the C++ STL all types are in `std`
    * We can overload operators for our types (quiz: what does
      the operator `<<` mean when applied to an integral type or in C?

* functions_and_namespaces.cpp
    * Definition of a simple function, nested in the `aau` namespace

* variables_and_loops.cpp
    * We have the same built-in types as in C
    * We can define variables anywhere.
    * Loops an other control flow statements also work (such as `if` and `else`)

* function_arguments.cpp
    * We can pass arguments to funtions
        * by value, reference, or pointer
    * We can also return values if needed

* classes.cpp
    * Classes or struct are interchangable
        * Only differenc is default access specifiers
    * Classes group functionaltiy and data
    * Objects are instances of a class
        * They can be allocated on the stack or on the heap

* classes_constructor.cpp
    * We can define constructors for initializing our objects
    * Constructors are special functions with the same name as the class
    * Destructors are responsible for cleaning up (if needed)

* classes_inheritance.cpp
    * Using inhertance one data type can acquire the properties of another
    * Virtual functions allow us to override functionality in the base class
    * Purely virtual functions allow us to define an interface

* classes_polymorphism.cpp
    * We can treat different objects the same though the interface/base.
    * We are not dealing with memory management issues here. Quiz:
      what should we do here to clean up?

* classes_polymorphism_containers.cpp
    * C++ offers a number of container types which allows us to store
      collections of ojects.
    * See an overview here: http://en.cppreference.com/w/cpp/container

* function_templates.cpp
    * Function templates allow us to define functions that work across a
      range of different types
    * Template argument types are deduced by the compiler

* class_templates.cpp
    * Classes may also have template arguments, this is in particiular when
      designing containers or other general purpose data structures