#include <iostream>
#include <string>
#include <vector>
#include <map>

class lecture
{
public: // Available to anybody

    // Return true if passed exam
    virtual bool take_exam() = 0;
};

class math_lecture : public lecture
{
public:

    virtual bool take_exam()
    {
        std::cout << "What is 2 + 2" << std::endl;
        int answer;
        std::cin >> answer;

        return answer == 4;
    }
};

class history_lecture : public lecture
{
public:

    virtual bool take_exam()
    {
        std::cout << "When was the first C++ standard released" << std::endl;
        int answer;
        std::cin >> answer;

        return answer == 1998;
    }
};


int main()
{
    std::vector<lecture*> lectures;

    lectures.push_back(new math_lecture());
    lectures.push_back(new history_lecture());

    for(int i = 0; i < lectures.size(); ++i)
    {
        std::cout << lectures[i]->take_exam() << std::endl;
    }

    return 0;
}
