#include <iostream>

namespace aau
{
    void print(int loops)
    {
        for(int i = 0; i < loops; ++i)
        {
            std::cout << "hello c++" << std::endl;
        }
    }
}

int main()
{
    int loops = 10;

    aau::print(loops);

    return 0;
}
