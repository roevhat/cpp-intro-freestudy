#include <iostream>

namespace aau
{
    void print()
    {
        std::cout << "hello c++" << std::endl;
    }
}

int main()
{
    aau::print();
    return 0;
}
