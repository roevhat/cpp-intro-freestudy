#include <iostream>
#include <string>

class lecture
{
public: // Available to anybody

    std::string date()
    {
        return "25 feb. 2015";
    }

    // Return true if passed exam
    virtual bool take_exam()
    {
        return true;
    }
};

class math_lecture : public lecture
{
public:

    virtual bool take_exam()
    {
        std::cout << "What is 2 + 2" << std::endl;
        int answer;
        std::cin >> answer;

        return answer == 4;
    }
};


int main()
{
    math_lecture lec1;
    std::cout << lec1.take_exam() << std::endl;

    return 0;
}
